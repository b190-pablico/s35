const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://ToffiePher22:Pherpogi22@wdc028-course-booking.ns5q7.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("we're connected to the database"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.post("/tasks", (req, res)=>{
	Task.findOne({name: req.body.name}, (err, result)=>{
		if (result !== null && result.name === req.body.name) {
			return res.send("Duplicate task found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask)=>{
				if (saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	});
})

app.get("/tasks", (req, res) =>{
	Task.find({}, (err, result) =>{
		if (err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

// SECTION: ACTIVITY
const userSchema = new mongoose.Schema({
	signup: {
		username: String,
		password: String
	},
})

const User = mongoose.model("User", userSchema);



app.post("/signup", (req, res) =>{
	User.findOne({ signup : req.body.signup }, (err, result) =>{
		if ( result !== null && result.signup === req.body.signup){
			return res.send("Duplicate user found");
		}else{
			let newUser = new User({
				signup: req.body.signup
			});
			newUser.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})

app.listen( port, () => console.log(`Server running at port: ${port}`));
